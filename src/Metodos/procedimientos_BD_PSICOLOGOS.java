package Metodos;

import static estructura.Gperfiles.TBADMI;
import static estructura.Gperfiles.TXTALABORABLE;
import static estructura.Gperfiles.TXTAPELLIDO;
import static estructura.Gperfiles.TXTDISTRITO;
import static estructura.Gperfiles.TXTDNI;
import static estructura.Gperfiles.TXTEDAD;
import static estructura.Gperfiles.TXTEMAIL;
import static estructura.Gperfiles.TXTESPECIALIDAD;
import static estructura.Gperfiles.TXTFECHA;
import static estructura.Gperfiles.TXTGACADEMICO;
import static estructura.Gperfiles.TXTNCOLEGIATURA;
import static estructura.Gperfiles.TXTNOMBRE;
import static estructura.Gperfiles.TXTPSALARIAL;
import static estructura.Gperfiles.TXTSEXO;
import static estructura.Gperfiles.TXTTELEFONO;
import static estructura.Gperfiles.lblfoto;
import java.awt.HeadlessException;
import java.awt.Image;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class procedimientos_BD_PSICOLOGOS {
    
    FileInputStream fis;
    int longitudBytes;
    static Connection conn = null;
    //PARA LA CONEXION
    static Statement s = null;
    //PARA ESTABLECER UNA CONSULTA
    static ResultSet rs = null;
    //PARA BRINDAR UN RESULTADO
    
    public void mostrar(DefaultTableModel modelo){
        try {
            //ejecuta la conexion
            conn = conexion.Enlace(conn);
            //ejecuta la consulta
            rs = conexion.PSICOLOGO(rs);
            //volcamos los resultados de rs a rsmetadata
            ResultSetMetaData rsMd = rs.getMetaData();
            //La cantidad de columnas que tiene la TABLA
            int cantidadColumnas = rsMd.getColumnCount();
            
            //Creando las filas para la tabla
            while (rs.next()) {
                Object[] fila = new Object[cantidadColumnas];
                for (int i = 0; i < cantidadColumnas-1; i++) {
                    fila[i] = rs.getObject(i + 1);
          
                }
                modelo.addRow(fila);
            }
            rs.close();
            conn.close();
        } 
        catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al mostrar \n" + ex);
        }
    }
    public void AgregarImagen(java.awt.event.ActionEvent evt) {                                                 
        lblfoto.setIcon(null);
        JFileChooser j=new JFileChooser();
        j.setFileSelectionMode(JFileChooser.FILES_ONLY);//solo archivos y no carpetas
        int estado=j.showOpenDialog(null);
        if(estado== JFileChooser.APPROVE_OPTION){
            try{
                fis=new FileInputStream(j.getSelectedFile());
                //necesitamos saber la cantidad de bytes
                this.longitudBytes=(int)j.getSelectedFile().length();
                try {
                    Image icono=ImageIO.read(j.getSelectedFile()).getScaledInstance
                            (lblfoto.getWidth(),lblfoto.getHeight(),Image.SCALE_DEFAULT);
                    lblfoto.setIcon(new ImageIcon(icono));
                    lblfoto.updateUI();

                } catch (IOException ex) {
                // JOptionPane showMessageDialog = JOptionPane.showMessageDialog(null, "imagen: "+ex);
                }
            }catch(FileNotFoundException ex){
            }
        }        // TOD
     } 
     public void registrar(DefaultTableModel modelo){
        try {
            conn = conexion.Enlace(conn);
            String sqlinsertar = "insert into PSICOLOGOS values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement psta = conn.prepareStatement(sqlinsertar);
            psta.setString(1, TXTNOMBRE.getText());
            psta.setString(2, TXTAPELLIDO.getText());
            psta.setString(3, (String)TXTESPECIALIDAD.getSelectedItem());
            psta.setString(4, TXTNCOLEGIATURA.getText());
            psta.setString(5, TXTDNI.getText());
            psta.setString(6, (String)TXTDISTRITO.getSelectedItem());
            psta.setString(7, TXTALABORABLE.getText());
            psta.setString(8, TXTFECHA.getText());
            psta.setString(9, TXTEDAD.getText());
            psta.setString(10, TXTPSALARIAL.getText());
            psta.setString(11, (String)TXTSEXO.getSelectedItem());
            psta.setString(12, TXTTELEFONO.getText());
            psta.setString(13, TXTEMAIL.getText());
            psta.setString(14, (String)TXTGACADEMICO.getSelectedItem());
            psta.setBinaryStream(15,fis,longitudBytes);
            psta.execute();
            psta.close();
        
            JOptionPane.showMessageDialog(null, "Datos se insertaron en la base de datos");
        } catch (SQLException | HeadlessException ex) {
            System.out.println(ex.getMessage());
        }

        String[] agregar = new String[14];
        agregar[0] = TXTNOMBRE.getText();
        agregar[1] = TXTAPELLIDO.getText();
        agregar[2] = (String)TXTESPECIALIDAD.getSelectedItem();
        agregar[3] = TXTNCOLEGIATURA.getText();
        agregar[4] = TXTDNI.getText();
        agregar[5] = (String)TXTDISTRITO.getSelectedItem();
        agregar[6] = TXTALABORABLE.getText();
        agregar[7] = TXTFECHA.getText();
        agregar[8] = TXTEDAD.getText();
        agregar[9] = TXTPSALARIAL.getText();
        agregar[10] = (String)TXTSEXO.getSelectedItem();
        agregar[11] = TXTTELEFONO.getText();
        agregar[12] = TXTEMAIL.getText();
        agregar[13] = (String)TXTGACADEMICO.getSelectedItem();
       
            
        modelo.addRow(agregar);
        
        TXTNOMBRE.setText("");
        TXTAPELLIDO.setText("");
        TXTESPECIALIDAD.setSelectedItem("Seleccionar:");
        TXTNCOLEGIATURA.setText("");
        TXTDISTRITO.setSelectedItem("Seleccionar:");
        TXTALABORABLE.setText("");
        TXTFECHA.setText("");
        TXTEDAD.setText("");
        TXTPSALARIAL.setText("");
        TXTSEXO.setSelectedItem("Seleccionar:");
        TXTTELEFONO.setText("");
        TXTEMAIL.setText("");
        TXTGACADEMICO.setSelectedItem("Seleccionar:");
        TXTDNI.setText("");
        lblfoto.setIcon(null);

    }
     
     public void devolverfoto(){
        String sql="select foto from PSICOLOGOS where DNI = "+TXTDNI.getText();
        ImageIcon foto;
        InputStream is;
        
        try{
            conn = conexion.Enlace(conn);
            s = conn.createStatement();
            rs = s.executeQuery(sql);
            
            if(rs.next()){
                is = rs.getBinaryStream(1);
                                
                BufferedImage bi = ImageIO.read(is);
                foto = new ImageIcon(bi);
                
                Image img = foto.getImage();
                Image newimg = img.getScaledInstance(140, 170, java.awt.Image.SCALE_SMOOTH);
                
                ImageIcon newicon = new ImageIcon(newimg);
                
                lblfoto.setIcon(newicon);
            }
        }catch(IOException | SQLException ex){
            JOptionPane.showMessageDialog(null,"exception: "+ex);
        }
    }
        
    public void editar(DefaultTableModel modelo){
        try {
            conn = conexion.Enlace(conn);
            System.out.println("1");
            String result = "UPDATE PSICOLOGOS SET NOMBRES='" + TXTNOMBRE.getText() + "'," 
                    + "APELLIDOS='" + TXTAPELLIDO.getText() + "',"
                    + "ESPECIALIDAD='" +(String)TXTESPECIALIDAD.getSelectedItem()+ "',"
                    + "NUM_COLEGIATURA=" + TXTNCOLEGIATURA.getText()+","
                    + "DISTRITO='" +((String)TXTDISTRITO.getSelectedItem()) + "',"
                    + "AÑOS_LABORAL=" + TXTALABORABLE.getText() + ","
                    + "PROPUESTA_SALARIAL=" + TXTPSALARIAL.getText() + ","
                    + "SEXO='" + ((String)TXTSEXO.getSelectedItem()) + "',"
                    + "TELEFONO=" + TXTTELEFONO.getText() + ","
                    + "EMAIL='" + TXTEMAIL.getText() + "',"
                    + "EDAD=" + TXTEDAD.getText() + ","
                    + "GRADO_ACADEMICO='" + (String)TXTGACADEMICO.getSelectedItem() + "'"
                    + "WHERE DNI =" +  TXTDNI.getText();
            
            PreparedStatement psta = conn.prepareStatement(result);
            psta.execute();
            psta.close();
    
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar \n" + e);
        }
    }    
    public void eliminar(DefaultTableModel modelo){
         int j = TBADMI.getSelectedRow();
         TXTDNI.setText(modelo.getValueAt(j, 4).toString());
      try {
            conn = conexion.Enlace(conn);
            String result = "DELETE FROM PSICOLOGOS WHERE DNI = " +TXTDNI.getText();
            PreparedStatement psta = conn.prepareStatement(result);

            psta.execute();
            psta.close();

             int elitotal=TBADMI.getRowCount();
            for (int i=elitotal-1; i>=0; i--) {
                modelo.removeRow(i);
            }
            mostrar(modelo);

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar \n" + e);
        }
    }     
}
