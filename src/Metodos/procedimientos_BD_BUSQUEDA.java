package Metodos;

import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class procedimientos_BD_BUSQUEDA {
        ListaPsicologos lista = new ListaPsicologos();
        String nombre, apellido, sexo,especialidad, distrito, aexp, psalaria,fnac,CMP, gacademico, Telfono, email,dni,edad;
        int can=0;
        //PARA LA IMAGEN
        ImageIcon newicon;
        FileInputStream fis;
        int longitudBytes;
        //PARA LA CONEXION
        static Connection conn = null;
        //PARA ESTABLECER UNA CONSULTA
        static Statement s = null;
        //PARA BRINDAR UN RESULTADO
        static ResultSet rs = null;
     
public void ObtenciondeDatos(DefaultTableModel modelo){
        try {
            //ejecuta la conexion
            conn = conexion.Enlace(conn);
            //ejecuta la consulta
            rs = conexion.BUSQUEDA(rs);
            //volcamos los resultados de rs a rsmetadata
            ResultSetMetaData rsMd = rs.getMetaData();
            //La cantidad de columnas que tiene la TABLA
            int cantidadColumnas = rsMd.getColumnCount();
            
            //Creando las filas para la tabla
            while (rs.next()) {
                Object[] fila = new Object[cantidadColumnas];
                for (int i = 0; i < cantidadColumnas; i++) {
                    fila[i] = rs.getObject(i + 1);
          
                }
                modelo.addRow(fila);
            }
            rs.close();
            conn.close();
        } 
        catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al mostrar \n" + ex);
        }
    }
    
public void recolecciondedatos(){
        try {
            //ejecuta la conexion
            conn = conexion.Enlace(conn);
            //ejecuta la consulta
            rs = conexion.PSICOLOGO(rs);
            //volcamos los resultados de rs a rsmetadata
            ResultSetMetaData rsMd1 = rs.getMetaData();
            //La cantidad de columnas que tiene la TABLA
            int cantidadColumnasPsicologo = rsMd1.getColumnCount();
                                    
            //Creando las filas para la tabla de busqueda con todos los campos 
            while (rs.next()) {
                Object[] fila2 = new Object[cantidadColumnasPsicologo];
                for (int i = 0; i < cantidadColumnasPsicologo; i++) {
                    fila2[i] = rs.getObject(i + 1);

                    if(i==0){
                      nombre = fila2[i].toString();
                    }if(i==1){
                      apellido = fila2[i].toString();  
                    }if(i==2){
                      especialidad = fila2[i].toString();  
                    }if(i==3){
                      CMP = fila2[i].toString();  
                    }if(i==4){
                      dni = fila2[i].toString();  
                    }if(i==5){
                      distrito = fila2[i].toString();  
                    }if(i==6){
                      aexp = fila2[i].toString();  
                    }if(i==7){
                      fnac = fila2[i].toString();  
                    }if(i==8){
                      edad = fila2[i].toString();  
                    }if(i==9){
                      psalaria = fila2[i].toString();  
                    }if(i==10){
                      sexo = fila2[i].toString();  
                    }if(i==11){
                      Telfono = fila2[i].toString();  
                    }if(i==12){
                      email = fila2[i].toString();  
                    }if(i==13){
                      gacademico = fila2[i].toString();  
                    }
          
                }
                lista.addnodo(nombre, apellido, sexo, especialidad, fnac, CMP, gacademico, Telfono, email, dni, distrito, aexp, psalaria,edad);
            }
                
            rs.close();
            conn.close();
        } 
        catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al mostrar \n" + ex);
        }
    }
    
public String[][] mostrar(){
              
             
        String matriz[][]=new String[lista.getTam()][7];
        for (int i = 0; i <lista.getTam(); i++) {
            
            matriz[i][0]=lista.mostrarNodo("nombre", i);
            matriz[i][1]=lista.mostrarNodo("apellido", i);
            matriz[i][2]=lista.mostrarNodo("sexo", i);
            matriz[i][3]=lista.mostrarNodo("especialidad", i);
            matriz[i][4]=lista.mostrarNodo("distrito", i);
            matriz[i][5]=lista.mostrarNodo("experiencia", i);
            matriz[i][6]=lista.mostrarNodo("salario", i);
   
        }
        return matriz;    
     }
 
public String[][] buscarXdis(String Distrito,int tam){
     String matriz[][]=new String[tam][7];
     int i = 0;
        for(int j =0; j < tam; j++){
            
            if(lista.mostrarNodo("distrito", j).compareTo(Distrito)==0){
            
            matriz[i][0]=lista.mostrarNodo("nombre", j);
            matriz[i][1]=lista.mostrarNodo("apellido", j);
            matriz[i][2]=lista.mostrarNodo("sexo", j);
            matriz[i][3]=lista.mostrarNodo("especialidad", j);
            matriz[i][4]=lista.mostrarNodo("distrito", j);
            matriz[i][5]=lista.mostrarNodo("experiencia", j);
            matriz[i][6]=lista.mostrarNodo("salario", j);
            i++;

            }
        }
            
        return matriz;
        
    }

public String[][] buscarXesp(String especialidad,int tam){
     String matriz[][]=new String[tam][7];
     int i = 0;
        for(int j =0; j < tam; j++){
            //.toUpperCase()->convierte el texto en mayuscula xq n el combobox esta en mayuscula
            if(lista.mostrarNodo("especialidad", j).toUpperCase().compareTo(especialidad)==0){
            
            matriz[i][0]=lista.mostrarNodo("nombre", j);
            matriz[i][1]=lista.mostrarNodo("apellido", j);
            matriz[i][2]=lista.mostrarNodo("sexo", j);
            matriz[i][3]=lista.mostrarNodo("especialidad", j);
            matriz[i][4]=lista.mostrarNodo("distrito", j);
            matriz[i][5]=lista.mostrarNodo("experiencia", j);
            matriz[i][6]=lista.mostrarNodo("salario", j);
            i++;
            
            }
        }
            
        return matriz;
        
    }

public String[][] buscarXpsal(String psalarial,int tam){
     String matriz[][]=new String[tam][7];
     StringTokenizer mistokens=new StringTokenizer(psalarial, "-");
     int min= Integer.parseInt(mistokens.nextToken());
      int max= Integer.parseInt(mistokens.nextToken());
        
       int i=0;
       int pos[][] = new int[tam][2];

    for(int j =0; j <tam; j++){

          if(Integer.parseInt(lista.mostrarNodo("salario", j))>=min && Integer.parseInt(lista.mostrarNodo("salario", j)) <=max){
//          
                pos[i][0]=Integer.parseInt(lista.mostrarNodo("salario", j));
                pos[i][1]=j;
                
             i++;
          
          }

        }

  
    for (int p = 1; p < i; p++){ 
             int aux = pos[p][0];
                int s = pos[p][1];
             int c = p - 1; 
              while ((c >= 0) && (aux < pos[c][0])){ 
                             pos[c + 1][0] = pos[c][0];     
                             pos[c + 1][1] = pos[c][1];
				c--;     

              }


              pos[c + 1][0] = aux; 
               pos[c + 1][1]= s;  
    }
     for (int p = 0; p < i; p++){
            matriz[p][0]=lista.mostrarNodo("nombre", pos[p][1]);
            matriz[p][1]=lista.mostrarNodo("apellido",pos[p][1]);
            matriz[p][2]=lista.mostrarNodo("sexo",pos[p][1]);
            matriz[p][3]=lista.mostrarNodo("especialidad", pos[p][1]);
            matriz[p][4]=lista.mostrarNodo("distrito",pos[p][1]);
            matriz[p][5]=lista.mostrarNodo("experiencia",pos[p][1]);
            matriz[p][6]=lista.mostrarNodo("salario", pos[p][1]);

     }
        return matriz;
    
}

private Image Obtenciondefoto(String DNI) {                                             
        String sql="select foto from PSICOLOGOS where DNI = "+DNI;
        ImageIcon foto;
        InputStream is;
        Image newimg = null;
        try{
            conn = conexion.Enlace(conn);
            s = conn.createStatement();
            rs = s.executeQuery(sql);
            
            while(rs.next()){
                is = rs.getBinaryStream(1);
                
                                
                BufferedImage bi = ImageIO.read(is);
                foto = new ImageIcon(bi);
                
                Image img = foto.getImage();
                newimg = img.getScaledInstance(140, 170, java.awt.Image.SCALE_SMOOTH);
            }
        }catch(IOException | SQLException ex){
            JOptionPane.showMessageDialog(null,"exception: "+ex);
        }
        
       return newimg;
    }
 
public void seleccionar(int row){
    //los demas componentes
    String NOMBRE=lista.mostrarNodo("nombre", row);
    String APELLIDO=lista.mostrarNodo("apellido", row);
    String ESPECIALIDAD=lista.mostrarNodo("especialidad", row);
    String EMAIL=lista.mostrarNodo("email", row);
    String SEXO=lista.mostrarNodo("sexo", row);
    String GRADO=lista.mostrarNodo("academico", row);
    String DISTRITO=lista.mostrarNodo("distrito", row);
    String cmp=lista.mostrarNodo("cmp", row);
    String DNI=lista.mostrarNodo("dni", row);
    String TELEFONO=lista.mostrarNodo("telefono", row);
    String EDAD=lista.mostrarNodo("edad", row);
    String SALARIO=lista.mostrarNodo("salario", row);
    String EXPERIENCIA=lista.mostrarNodo("experiencia", row);
    String FECHA=lista.mostrarNodo("fecha", row);
    System.out.println("DNI="+DNI);
    //paralaimagen
    Image newimg = Obtenciondefoto(DNI);
    newicon = new ImageIcon(newimg); 
    
    int seleccion = JOptionPane.showOptionDialog(
       null,
       "Nombre: "+NOMBRE
        +"\nApellidos: "+APELLIDO //El mensaje que aparece en el dialog
        +"\nEspecialidad: "+ESPECIALIDAD
        +"\nE - mail: "+EMAIL
        +"\nSexo: "+SEXO
        +"\nGrado Academico: "+GRADO
        +"\nDistrito: "+DISTRITO
        +"\nCMP: "+cmp
        +"\nDNI: "+DNI
        +"\nTELEFONO: "+TELEFONO
        +"\nFECHA DE NAC: "+FECHA
        +"\nEdad: "+EDAD
        +"\nPropuesta Salarial: "+SALARIO
        +"\nAños de experiencia: "+EXPERIENCIA
, 
       "Perfil de Usuario",
       JOptionPane.YES_NO_CANCEL_OPTION,//Botones que apareces para seleccionar
       JOptionPane.QUESTION_MESSAGE,//Icono por defecto
       newicon,    // para icono por defecto, o un icono personalizado
       new Object[] { "Añadir a mi lista de psicologos de preferencia", "Salir"},   // botones presonalizados  null 
                                                              //botones por defecto en este caso
                                                              // YES, NO y CANCEL
       "opcion 1");

    if(seleccion == 0){
       int escoger = JOptionPane.showConfirmDialog(null, "Esta seguro que desea añadir al psicologo", "Confirmacion de impresion", JOptionPane.YES_NO_OPTION);

        switch (escoger) {
            case JOptionPane.YES_OPTION:
                registrar(DNI);
                RegistraraSeleccionados(DNI);
                JOptionPane.showMessageDialog(null,"La informacion a sido añadida a la lista \n Gracias por escoger a uno de nuestros psicologos :)");
                break;
            case JOptionPane.NO_OPTION:

        }
    }
}

public void eliminar(String Tabla){
   try {
            conn = conexion.Enlace(conn);

            String sqlinsertar = "DELETE FROM "+Tabla;
            PreparedStatement psta = conn.prepareStatement(sqlinsertar);
            psta.execute();

            String result = "COMMIT";
            PreparedStatement psta1 = conn.prepareStatement(result);
            psta1.execute();
            
            psta.close();
            psta1.close();
            
        } catch (SQLException | HeadlessException ex) {
            System.out.println(ex.getMessage());
        } 
}

public void registrar(String DNI){
    
    try {
        conn = conexion.Enlace(conn);

        String sqlinsertar = "INSERT INTO REPORTES(NOMBRES,APELLIDOS,ESPECIALIDAD,NUM_COLEGIATURA,DNI,DISTRITO,AÑOS_LABORAL,FECHA_NAC,EDAD,PROPUESTA_SALARIAL,SEXO,TELEFONO,EMAIL,GRADO_ACADEMICO,FOTO)" +
                             " SELECT * FROM PSICOLOGOS WHERE DNI = "+DNI;
        PreparedStatement psta = conn.prepareStatement(sqlinsertar);
        psta.execute();

        String result = "COMMIT";
        PreparedStatement psta1 = conn.prepareStatement(result);
        psta1.execute();

        psta.close();
        psta1.close();

    } catch (SQLException | HeadlessException ex) {
        System.out.println(ex.getMessage());
    }
}

public void RegistraraSeleccionados(String DNI){
    
    try {
        conn = conexion.Enlace(conn);

        String sqlinsertar = "INSERT INTO REPORTECLIENTE(NOMBRES,APELLIDOS,ESPECIALIDAD,NUM_COLEGIATURA,DNI,DISTRITO,AÑOS_LABORAL,FECHA_NAC,EDAD,PROPUESTA_SALARIAL,SEXO,TELEFONO,EMAIL,GRADO_ACADEMICO,FOTO)" +
                             " SELECT * FROM PSICOLOGOS WHERE DNI = "+DNI;
        PreparedStatement psta = conn.prepareStatement(sqlinsertar);
        psta.execute();

        String result = "COMMIT";
        PreparedStatement psta1 = conn.prepareStatement(result);
        psta1.execute();

        psta.close();
        psta1.close();

    } catch (SQLException | HeadlessException ex) {
        JOptionPane.showMessageDialog(null,"A habido un error vuelva a intentarlo");
        System.out.println(ex.getMessage());
    }
}     

public void Impresion(String ruta){
    procedimiento_BD_REPORTES reporte = new procedimiento_BD_REPORTES();
    String SQL="SELECT * FROM REPORTECLIENTE";

    try {

        conn = conexion.Enlace(conn);
        s = conn.createStatement();
        rs = s.executeQuery(SQL);

        if(rs.next()){
            can++;
        }
        
        conn.close();

    } catch (SQLException ex) {

        JOptionPane.showMessageDialog(null, ex, "Error de conexión", JOptionPane.ERROR_MESSAGE);

    }
    
    if(can!=0){
        reporte.ReportarPDF(ruta);
    }else{
       JOptionPane.showMessageDialog(null,"No tiene ningun psicologo seleccionado"); 
    }
}

}
