package Metodos;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

public class procedimiento_BD_REPORTES {
    static Connection conn = null;
    //PARA LA CONEXION
    static Statement s = null;
    //PARA ESTABLECER UNA CONSULTA
    static ResultSet rs = null;
    //PARA BRINDAR UN RESULTADO
    static Connection conn2=null;
    
    procedimientos_BD_BUSQUEDA bsqd = new procedimientos_BD_BUSQUEDA(); 
    String REPORTES;
    
    public procedimiento_BD_REPORTES() {
        
    }
    
    //METODO PARA EXPORTAR DATOS DE LA TABLA REPORTES
    
    public void mostrar(DefaultTableModel modelo){
        try {
            //ejecuta la conexion
            conn = conexion.Enlace(conn);
            //ejecuta la consulta
            rs = conexion.REPORTES(rs);
            //volcamos los resultados de rs a rsmetadata
            ResultSetMetaData rsMd = rs.getMetaData();
            //La cantidad de columnas que tiene la TABLA
            int cantidadColumnas = rsMd.getColumnCount();
            
            //Creando las filas para la tabla
            while (rs.next()) {
                Object[] fila = new Object[cantidadColumnas];
                for (int i = 0; i < cantidadColumnas; i++) {
                    fila[i] = rs.getObject(i + 1);
          
                }
                modelo.addRow(fila);
            }
            rs.close();
            conn.close();
        } 
        catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al mostrar \n" + ex);
        }
    }
    
    //METODO PARA EXPORTAR A PDF UN REPORTE   

    public void ReportarPDF(String ruta){
         Reportes re = new Reportes();//CREAMOS UN OBJETO DE LA CLASE REPORTES
         //ABRIR CUADRO DE DIALOGO PARA GUARDAR EL ARCHIVO         
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("todos los archivos *.PDF", "pdf", "PDF"));//filtro para ver solo archivos .pdf
        int seleccion = fileChooser.showSaveDialog(null);
        try {
            if (seleccion == JFileChooser.APPROVE_OPTION) {//comprueba si ha presionado el boton de aceptar
                File JFC = fileChooser.getSelectedFile();
                String PATH = JFC.getAbsolutePath();//obtenemos la direccion del archivo + el nombre a guardar
                try (PrintWriter printwriter = new PrintWriter(JFC)) {
                    printwriter.print(ruta);
                }
                re.resportesPDF(ruta, PATH);//mandamos como parametros la ruta del archivo a compilar y el nombre y ruta donde se guardaran    
                //comprobamos si a la hora de guardar obtuvo la extension y si no se la asignamos
                if (!(PATH.endsWith(".pdf"))) {
                    File temp = new File(PATH + ".pdf");
                    JFC.renameTo(temp);//renombramos el archivo
                }
                JOptionPane.showMessageDialog(null, "Esto puede tardar unos segundos,espere porfavor", "Estamos Generando el Reporte", JOptionPane.WARNING_MESSAGE);
                JOptionPane.showMessageDialog(null, "Documento Exportado Exitosamente!", "Guardado exitoso!", JOptionPane.INFORMATION_MESSAGE);
            }
            
        } catch (FileNotFoundException | HeadlessException e) {//por alguna excepcion salta un mensaje de error
            JOptionPane.showMessageDialog(null, "Error al Exportar el archivo!", "Oops! Error", JOptionPane.ERROR_MESSAGE);
        }   
    }
    
    //METODO PARA REALIZAR UN NUEVO REGISTRO
    
    public void NuevoReporte(){
        int can =0;
        
        try {

           String SQL="SELECT * FROM REPORTES";
            conn = conexion.Enlace(conn);
            s = conn.createStatement();
            rs = s.executeQuery(SQL);

            if(rs.next()){
                can++;
            }

            conn.close();

        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex, "Error de conexión", JOptionPane.ERROR_MESSAGE);

        }

        if(can!=0){
           int escoger = JOptionPane.showConfirmDialog(null, "¿Desea borrar el registro e iniciar uno nuevo?", "Iniciacion de nuevo registro ", JOptionPane.YES_NO_OPTION);
           
           
            switch (escoger) {
                case JOptionPane.YES_OPTION:
                    bsqd.eliminar(REPORTES);
                    JOptionPane.showMessageDialog(null,"La informacion del dia a sido borrada .. Se iniciara un nuevo registro :)");
                    break;
                case JOptionPane.NO_OPTION:

            }
        }else{
           JOptionPane.showMessageDialog(null,"El registro esta vacio"); 
        }
    }
    
    
}
