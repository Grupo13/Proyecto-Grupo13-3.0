
package Metodos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class Reportes {

    Connection conexion;
    Statement instrucion;

    public Reportes() {
        try {
            Class.forName("oracle.jdbc.OracleDriver");
            //Creamos un enlace hacia la base de datos
            conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:DATABASE2123", "michelle", "michelle2123");
            instrucion = conexion.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, e, "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
        }
    }

//METODO PARA EXPORTAR A PDF UN REPORTE   

    public void resportesPDF(String ruta, String archi) {
        try {
//            String rutaInforme = ruta;
//          Tipo impresion = Tipomanejador de archivos reporte lleno
            JasperPrint informe = JasperFillManager.fillReport(ruta, null, conexion);
            JasperExportManager.exportReportToPdfFile(informe, archi);

            JasperViewer ventanavisor = new JasperViewer(informe, false);
            ventanavisor.setTitle("INFORME");
            ventanavisor.setVisible(false);
        } catch (JRException e) {
            JOptionPane.showMessageDialog(null, "ERROR AL EXPORTAR DOCUMENTO");
        }
    }

   
}
