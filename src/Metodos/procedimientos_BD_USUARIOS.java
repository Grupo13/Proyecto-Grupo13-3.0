package Metodos;

import static Metodos.conexion.conn;
import static Metodos.conexion.rs;
import static estructura.GestiondeUsuarios.TBADMI;
import static estructura.GestiondeUsuarios.TXTApellido;
import static estructura.GestiondeUsuarios.TXTContraseña;
import static estructura.GestiondeUsuarios.TXTDNI;
import static estructura.GestiondeUsuarios.TXTECivil;
import static estructura.GestiondeUsuarios.TXTNombre;
import static estructura.GestiondeUsuarios.TXTOcupacion;
import static estructura.GestiondeUsuarios.TXTSexo;
import static estructura.GestiondeUsuarios.TXTUsuario;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class procedimientos_BD_USUARIOS {
    
    public void mostrar(DefaultTableModel modelo){
        try {
            //ejecuta la conexion
            conn = conexion.Enlace(conn);
            //ejecuta la consulta
            rs = conexion.USUARIO(rs);
            //volcamos los resultados de rs a rsmetadata
            ResultSetMetaData rsMd = rs.getMetaData();
            //La cantidad de columnas que tiene la TABLA
            int cantidadColumnas = rsMd.getColumnCount();
            
            //Creando las filas para la tabla
            while (rs.next()) {
                Object[] fila = new Object[cantidadColumnas];
                for (int i = 0; i < cantidadColumnas; i++) {
                    fila[i] = rs.getObject(i + 1);
             }
//                    System.out.println(" Otro ");
//                    System.out.println(fila);
                modelo.addRow(fila);
            }
            rs.close();
            conn.close();
        } 
        catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al mostrar \n" + ex);
        }
    }
    
   public void registrar(DefaultTableModel modelo){
        try {
            conn = conexion.Enlace(conn);
            String sqlinsertar = "insert into USUARIO values (?,?,?,?,?,?,?,?)";
            PreparedStatement psta = conn.prepareStatement(sqlinsertar);
            psta.setString(1, TXTNombre.getText());
            psta.setString(2, TXTApellido.getText());
            psta.setString(3, TXTDNI.getText());
            psta.setString(4, TXTUsuario.getText());
            psta.setString(5, TXTContraseña.getText());
            psta.setString(6, (String)TXTSexo.getSelectedItem());
            psta.setString(7, TXTECivil.getText());
            psta.setString(8, (String)TXTOcupacion.getSelectedItem());
            psta.execute();
            psta.close();
            
            JOptionPane.showMessageDialog(null, "Datos se insertaron en la base de datos");
        } catch (SQLException | HeadlessException ex) {
            System.out.println(ex.getMessage());
        }
//PARA EL FRAME 
        String[] agregar = new String[8];
        agregar[0] = TXTNombre.getText();
        agregar[1] = TXTApellido.getText();
        agregar[2] = TXTDNI.getText();
        agregar[3] = TXTUsuario.getText();
        agregar[4] = TXTContraseña.getText();
        agregar[5] = (String)TXTSexo.getSelectedItem();
        agregar[6] = TXTECivil.getText();
        agregar[7] = (String)TXTOcupacion.getSelectedItem();
        modelo.addRow(agregar);
        
        TXTNombre.setText("");
        TXTApellido.setText("");
        TXTDNI.setText("");
        TXTUsuario.setText("");
        TXTContraseña.setText("");
        TXTSexo.setSelectedItem("Seleccionar:");
        TXTECivil.setText("");
        TXTOcupacion.setSelectedItem("Seleccionar:");

    }
        
    public void editar(DefaultTableModel modelo){
        try {
            conn = conexion.Enlace(conn);
            String result = "UPDATE USUARIO SET NOMBRE='" + TXTNombre.getText() + "',"
                            + "APELLIDO='" + TXTApellido.getText() + "',"
                            + "CARGO_OCUPACIONAL='" +((String)TXTOcupacion.getSelectedItem())+ "',"
                            + "USUARIO='" + TXTUsuario.getText()+"',"
                            + "CONTRASEÑA='" + TXTContraseña.getText() + "',"
                            + "SEXO='" + ((String)TXTSexo.getSelectedItem()) + "',"
                            + "ESTADO_CIVIL='" + TXTECivil.getText() + "'"
                            + "WHERE DNI =" +  TXTDNI.getText();
            System.out.println(result);
            PreparedStatement psta = conn.prepareStatement(result);
            

            psta.execute();
            psta.close();

            int elitotal = TBADMI.getRowCount();
            for (int i = elitotal - 1; i >= 0; i--) {
                modelo.removeRow(i);
            }
            mostrar(modelo);

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar \n" + e);
        }      
    }
    public void eliminar(DefaultTableModel modelo){
       int j = TBADMI.getSelectedRow();
       TXTDNI.setText(modelo.getValueAt(j, 2).toString());
        
       
        try {
            conn = conexion.Enlace(conn);
            String result = "DELETE FROM USUARIO WHERE DNI = " +TXTDNI.getText();
            PreparedStatement psta = conn.prepareStatement(result);
            psta.execute();
            psta.close();

            
            int elitotal=TBADMI.getRowCount();
            for (int i=elitotal-1; i>=0; i--) {
                modelo.removeRow(i);
            }
            mostrar(modelo);

        } 
        catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar \n" + e);
        }
    }
    
     
               
}
