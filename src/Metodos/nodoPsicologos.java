
package Metodos;

import javax.swing.Icon;

public class nodoPsicologos{

    private String nombre, apellido, sexo,especialidad, distrito, aexp, psalaria,fnac,CMP, gacademico, Telfono, email,dni,edad;
   //  private Icon img;
   
     private nodoPsicologos siguiente ;
 
    
 
    public nodoPsicologos() {
        
        //crea el primer nodo
        this.siguiente = null;
    } 
    public nodoPsicologos getSiguiente() {
        return siguiente;
    }

    
    public void setSiguiente(nodoPsicologos siguiente) {
        this.siguiente = siguiente;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getSexo() {
        return sexo;
    }
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
  
    public String getEspecialidad() {
        return especialidad;
    }
    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getDistrito() {
        return distrito;
    }
    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getAexp() {
        return aexp;
    }
     public void setAexp(String aexp) {
        this.aexp = aexp;
    }

    public String getPsalaria() {
        return psalaria;
    }
    public void setPsalaria(String psalaria) {
        this.psalaria = psalaria;
    }

    public String getFnac() {
        return fnac;
    }
    public void setFnac(String fnac) {
        this.fnac = fnac;
    }

    public String getCMP() {
        return CMP;
    }
    public void setCMP(String CMP) {
        this.CMP = CMP;
    }

    public String getGacademico() {
        return gacademico;
    }
    public void setGacademico(String gacademico) {
        this.gacademico = gacademico;
    }

    public String getTelfono() {
        return Telfono;
    }
    public void setTelfono(String Telfono) {
        this.Telfono = Telfono;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }


    public String getDni() {
        return dni;
    }
    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getEdad() {
        return edad;
    }
    public void setEdad(String edad) {
        this.edad = edad;
    }
     

   
}