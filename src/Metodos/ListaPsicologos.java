
package Metodos;

import java.io.IOException;
import java.util.Scanner;
import javax.swing.JFrame;


public class ListaPsicologos extends JFrame {


    private javax.swing.ImageIcon imagen= new javax.swing.ImageIcon();
    private int tam;
    private nodoPsicologos primero, ultimo;
    
public ListaPsicologos() {
this.primero = null; 
this.ultimo=null;
} 

public void addnodo(String nombre,
                    String ape,
                    String sexo,
                    String especialidad,
                    String fnac, 
                    String CMP,
                    String gacademico,
                    String telefono,
                    String email,
                    String dni,
                    String distrito, 
                    String aexp,
                    String psalarial,
                    String edad
                    ){
    
    nodoPsicologos nuevo = new nodoPsicologos();
    if(this.primero==null){
      
    nuevo.setNombre(nombre);
    nuevo.setApellido(ape);
    nuevo.setSexo(sexo);
    nuevo.setDistrito(distrito);
    nuevo.setEspecialidad(especialidad);
    nuevo.setAexp(aexp);
    nuevo.setPsalaria(psalarial);
    nuevo.setFnac(fnac);
    nuevo.setCMP(CMP);
    nuevo.setGacademico(gacademico);
    nuevo.setTelfono(telefono);
    nuevo.setEmail(email);
    nuevo.setDni(dni);
    nuevo.setEdad(edad);
   // nuevo.setImg(imagen));
    
    nuevo.setSiguiente(primero);
    primero = nuevo;   
    ultimo = nuevo;
    }else{
    nuevo.setNombre(nombre);
    nuevo.setApellido(ape);
    nuevo.setSexo(sexo);
    nuevo.setDistrito(distrito);
    nuevo.setEspecialidad(especialidad);
    nuevo.setAexp(aexp);
    nuevo.setPsalaria(psalarial);
    nuevo.setFnac(fnac);
    nuevo.setCMP(CMP);
    nuevo.setGacademico(gacademico);
    nuevo.setTelfono(telefono);
    nuevo.setEmail(email);
    nuevo.setDni(dni);
    nuevo.setEdad(edad);
    // nuevo.setImg(imagen));
    
    nuevo.setSiguiente(null);  
    ultimo.setSiguiente(nuevo);
    ultimo = nuevo;
    }
   
    tam++;
}

public String mostrarNodo(String retorno, int posfila){
         nodoPsicologos actual = primero;
         int i=0;

    while(i<posfila && actual !=null){
    actual = actual.getSiguiente();
    i++;

    }     


    try{
    switch(retorno){
        case "nombre":
            return actual.getNombre();
           
        case "apellido":
          return actual.getApellido();
          
         case "sexo":
          return actual.getSexo();
          
           case "distrito":
          return actual.getDistrito();
          
           case "especialidad":
          return actual.getEspecialidad();
           
           case "experiencia":
          return actual.getAexp();
          
           case "salario":
          return actual.getPsalaria();
          
           case "nacimiento":
          return actual.getFnac();
          
           case "cmp":
          return actual.getCMP();
          
           case "academico":
          return actual.getGacademico();
          
           case "telefono":
          return actual.getTelfono();
          
           case "email":
          return actual.getEmail();
           case "dni":
          return actual.getDni();
          case "edad":
          return actual.getEdad();
          case "fecha":
          return actual.getFnac();
          
          
        default:
                  return "valor mal ingresado";
    }
    }catch(Exception e){
     return " ";   
    }
}

    public int getTam() {
        return tam;
    }
}



