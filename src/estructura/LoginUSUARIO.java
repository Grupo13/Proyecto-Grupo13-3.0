package estructura;


import Metodos.MetodosLoginUsuario;
import javax.swing.JOptionPane;
public class LoginUSUARIO extends javax.swing.JFrame {

    public LoginUSUARIO() {
        initComponents();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Ingresar = new javax.swing.JButton();
        jpassClave = new javax.swing.JPasswordField();
        txtUsuario = new javax.swing.JTextField();
        Salir = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Ingresar.setBackground(new java.awt.Color(0, 153, 51));
        Ingresar.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        Ingresar.setText("INGRESAR");
        Ingresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IngresarActionPerformed(evt);
            }
        });
        getContentPane().add(Ingresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 380, 140, 30));

        jpassClave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jpassClaveActionPerformed(evt);
            }
        });
        getContentPane().add(jpassClave, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 290, 150, 43));

        txtUsuario.setPreferredSize(new java.awt.Dimension(18, 20));
        txtUsuario.setRequestFocusEnabled(false);
        txtUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUsuarioActionPerformed(evt);
            }
        });
        getContentPane().add(txtUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 210, 150, 43));

        Salir.setBackground(new java.awt.Color(255, 0, 0));
        Salir.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        Salir.setText("SALIR");
        Salir.setPreferredSize(new java.awt.Dimension(111, 31));
        Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalirActionPerformed(evt);
            }
        });
        getContentPane().add(Salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 430, 140, 30));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes2/0/ultimo login.jpg"))); // NOI18N
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(-10, 0, -1, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void IngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IngresarActionPerformed
        MetodosLoginUsuario metodoslogin = new MetodosLoginUsuario();
        if(metodoslogin.validar_ingreso()==1){
                  
                    this.dispose();

                    JOptionPane.showMessageDialog(null, "Bienvenido\n Has ingresado "
                    + "satisfactoriamente al sistema", "Mensaje de bienvenida",
                    JOptionPane.INFORMATION_MESSAGE);

                     Eleccion3 formformularioUsuario = new  Eleccion3();
                    formformularioUsuario.setLocationRelativeTo(null);
                    formformularioUsuario.setVisible(true);
                    dispose();

        }else {
                    
                    JOptionPane.showMessageDialog(null, "Acceso denegado:\n"
                    + "Por favor ingrese un usuario y/o contraseña correctos", "Acceso denegado",
                    JOptionPane.ERROR_MESSAGE);
            
        }
    }//GEN-LAST:event_IngresarActionPerformed

    private void txtUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUsuarioActionPerformed
     
    }//GEN-LAST:event_txtUsuarioActionPerformed

    private void SalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalirActionPerformed
        Eleccion2 m=new Eleccion2();
        m.setLocationRelativeTo(null);
        m.setVisible(true);
        dispose();         
    }//GEN-LAST:event_SalirActionPerformed

    private void jpassClaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jpassClaveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jpassClaveActionPerformed

    public static void main(String args[]) {
   
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginUSUARIO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
              java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new LoginUSUARIO().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Ingresar;
    private javax.swing.JButton Salir;
    private javax.swing.JLabel jLabel4;
    public static javax.swing.JPasswordField jpassClave;
    public static javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
