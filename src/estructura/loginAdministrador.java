package estructura;

import Metodos.MetodosLoginAdministrador;
import javax.swing.JOptionPane;

public class loginAdministrador extends javax.swing.JFrame {

    public loginAdministrador() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        INGRESAR = new javax.swing.JButton();
        PasswordADMIN = new javax.swing.JPasswordField();
        SALIR = new javax.swing.JButton();
        txtADMIN = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        INGRESAR.setBackground(new java.awt.Color(0, 153, 51));
        INGRESAR.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        INGRESAR.setText("INGRESAR");
        INGRESAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                INGRESARActionPerformed(evt);
            }
        });
        getContentPane().add(INGRESAR, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 380, 140, 30));

        PasswordADMIN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PasswordADMINActionPerformed(evt);
            }
        });
        getContentPane().add(PasswordADMIN, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 290, 150, 43));

        SALIR.setBackground(new java.awt.Color(255, 0, 0));
        SALIR.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        SALIR.setText("SALIR");
        SALIR.setPreferredSize(new java.awt.Dimension(111, 31));
        SALIR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SALIRActionPerformed(evt);
            }
        });
        getContentPane().add(SALIR, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 440, 140, 30));

        txtADMIN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtADMINActionPerformed(evt);
            }
        });
        getContentPane().add(txtADMIN, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 210, 150, 43));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes2/0/ultimo login.jpg"))); // NOI18N
        jLabel1.setToolTipText("");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtADMINActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtADMINActionPerformed

    }//GEN-LAST:event_txtADMINActionPerformed

    private void PasswordADMINActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PasswordADMINActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PasswordADMINActionPerformed

    private void SALIRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SALIRActionPerformed
        Eleccion2 m=new Eleccion2();
        m.setLocationRelativeTo(null);
        m.setVisible(true);
        dispose();
    }//GEN-LAST:event_SALIRActionPerformed

    private void INGRESARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_INGRESARActionPerformed
        MetodosLoginAdministrador metodosloginadmi = new MetodosLoginAdministrador();
        if(metodosloginadmi.validar_ingreso()==1){

            this.dispose();

            JOptionPane.showMessageDialog(null, "Bienvenido\n Has ingresado "
                + "satisfactoriamente al sistema", "Mensaje de bienvenida",
                JOptionPane.INFORMATION_MESSAGE);

            GestiondeUsuarios formformularioAdmi = new GestiondeUsuarios();
            formformularioAdmi.setLocationRelativeTo(null);
            formformularioAdmi.setVisible(true);
            dispose();

        }else {

            JOptionPane.showMessageDialog(null, "Acceso denegado:\n"
                + "Por favor ingrese un usuario y/o contraseña correctos", "Acceso denegado",
                JOptionPane.ERROR_MESSAGE);

        }

    }//GEN-LAST:event_INGRESARActionPerformed

    public static void main(String args[]) {
      
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(loginAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
      
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new loginAdministrador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton INGRESAR;
    public static javax.swing.JPasswordField PasswordADMIN;
    private javax.swing.JButton SALIR;
    private javax.swing.JLabel jLabel1;
    public static javax.swing.JTextField txtADMIN;
    // End of variables declaration//GEN-END:variables
}
