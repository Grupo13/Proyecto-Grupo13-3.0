
package estructura;

import Metodos.conexion;
import Metodos.procedimientos_BD_USUARIOS;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.table.DefaultTableModel;

public final class GestiondeUsuarios extends javax.swing.JFrame {

    //Para establecer el modelo al JTable
    procedimientos_BD_USUARIOS usuario = new procedimientos_BD_USUARIOS();
    DefaultTableModel modelo = new DefaultTableModel();
    
    static Connection conn = null;
    //PARA LA CONEXION
    static Statement s = null;
    //PARA ESTABLECER UNA CONSULTA
    static ResultSet rs = null;
    //PARA BRINDAR UN RESULTADO
    
    public GestiondeUsuarios() {
        initComponents();
        try {
            //ejecuta la conexion
            conn = conexion.Enlace(conn);
            //ejecuta la consulta
            rs = conexion.USUARIO(rs);
            //volcamos los resultados de rs a rsmetadata
            ResultSetMetaData rsMd = rs.getMetaData();
            //La cantidad de columnas que tiene la TABLA
            int cantidadColumnas = rsMd.getColumnCount();
            //Establecer como cabezeras el nombre de las colunnas
            for (int i = 1; i <= cantidadColumnas; i++) {
                modelo.addColumn(rsMd.getColumnLabel(i));
            }
            rs.close();
            conn.close();
        } catch (SQLException ex) {
        }
       usuario.mostrar(modelo);
        GestiondeUsuarios.TBADMI.setModel(modelo);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        TBADMI = new javax.swing.JTable();
        TXTNombre = new javax.swing.JTextField();
        BTNAGREGAR = new javax.swing.JButton();
        BTNELIMINAR = new javax.swing.JButton();
        BTNEDITAR = new javax.swing.JButton();
        btnACTUALIZAR = new javax.swing.JButton();
        BTNSALIRADMI = new javax.swing.JButton();
        TXTContraseña = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        TXTApellido = new javax.swing.JTextField();
        TXTECivil = new javax.swing.JTextField();
        TXTDNI = new javax.swing.JTextField();
        TXTUsuario = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        TXTSexo = new javax.swing.JComboBox<>();
        BTNMODIFICAR = new javax.swing.JButton();
        TXTOcupacion = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        TBADMI.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "NOMBRE", "APELLIDOS", "DNI", "USUARIO", "CONTRASEÑA", "SEXO", "ESTADO CIVIL", "OCUPACION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, true, false, false, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TBADMI.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane1.setViewportView(TBADMI);

        TXTNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTNombreActionPerformed(evt);
            }
        });

        BTNAGREGAR.setBackground(new java.awt.Color(255, 255, 255));
        BTNAGREGAR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes2/0/agregarf.png"))); // NOI18N
        BTNAGREGAR.setBorder(null);
        BTNAGREGAR.setBorderPainted(false);
        BTNAGREGAR.setContentAreaFilled(false);
        BTNAGREGAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNAGREGARActionPerformed(evt);
            }
        });

        BTNELIMINAR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes2/0/deletef.png"))); // NOI18N
        BTNELIMINAR.setBorder(null);
        BTNELIMINAR.setBorderPainted(false);
        BTNELIMINAR.setContentAreaFilled(false);
        BTNELIMINAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNELIMINARActionPerformed(evt);
            }
        });

        BTNEDITAR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes2/0/editf.png"))); // NOI18N
        BTNEDITAR.setBorder(null);
        BTNEDITAR.setBorderPainted(false);
        BTNEDITAR.setContentAreaFilled(false);
        BTNEDITAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNEDITARActionPerformed(evt);
            }
        });

        btnACTUALIZAR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes2/0/refreshf.png"))); // NOI18N
        btnACTUALIZAR.setBorder(null);
        btnACTUALIZAR.setBorderPainted(false);
        btnACTUALIZAR.setContentAreaFilled(false);
        btnACTUALIZAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnACTUALIZARActionPerformed(evt);
            }
        });

        BTNSALIRADMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes2/0/EXITF2.png"))); // NOI18N
        BTNSALIRADMI.setBorder(null);
        BTNSALIRADMI.setBorderPainted(false);
        BTNSALIRADMI.setContentAreaFilled(false);
        BTNSALIRADMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNSALIRADMIActionPerformed(evt);
            }
        });

        TXTContraseña.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTContraseñaActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 3, 36)); // NOI18N
        jLabel11.setText("REGISTROS DE USUARIOS");

        TXTApellido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTApellidoActionPerformed(evt);
            }
        });

        TXTECivil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTECivilActionPerformed(evt);
            }
        });

        TXTDNI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTDNIActionPerformed(evt);
            }
        });

        TXTUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTUsuarioActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel2.setText("Nombres");

        jLabel3.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel3.setText("Usuario");

        jLabel4.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel4.setText("Apellidos");

        jLabel5.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel5.setText("Contraseña");

        jLabel6.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel6.setText("Estado Civil");

        jLabel7.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel7.setText("DNI");

        jLabel8.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel8.setText("Sexo");

        jLabel9.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel9.setText("Ocupación");

        TXTSexo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        TXTSexo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar:", "MASCULINO", "FEMENINO" }));
        TXTSexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTSexoActionPerformed(evt);
            }
        });

        BTNMODIFICAR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes2/0/Saveff.png"))); // NOI18N
        BTNMODIFICAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNMODIFICARActionPerformed(evt);
            }
        });

        TXTOcupacion.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        TXTOcupacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar:", "Administrador", "Secretaria/o", "Presidente", "" }));
        TXTOcupacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTOcupacionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(203, 203, 203)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(TXTOcupacion, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(69, 69, 69)
                .addComponent(jLabel8)
                .addGap(41, 41, 41)
                .addComponent(TXTSexo, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(BTNAGREGAR, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnACTUALIZAR, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BTNEDITAR, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BTNELIMINAR, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(17, 17, 17)
                                .addComponent(TXTUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(57, 57, 57))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(TXTNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(54, 54, 54)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(TXTContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(TXTApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(61, 61, 61)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(18, 18, 18)
                                .addComponent(TXTECivil, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(18, 18, 18)
                                .addComponent(TXTDNI, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(BTNSALIRADMI, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(244, 244, 244)
                        .addComponent(jLabel11)))
                .addContainerGap(27, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(BTNMODIFICAR)
                        .addGap(345, 345, 345))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 617, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(153, 153, 153))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(BTNAGREGAR, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BTNELIMINAR, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(BTNEDITAR, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(btnACTUALIZAR, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BTNSALIRADMI, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(TXTNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(TXTApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(TXTUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(TXTContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(TXTDNI, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(41, 41, 41)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(TXTECivil, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TXTOcupacion, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(TXTSexo, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BTNMODIFICAR, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BTNELIMINARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNELIMINARActionPerformed
       usuario.eliminar(modelo);
    }//GEN-LAST:event_BTNELIMINARActionPerformed

    private void TXTNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TXTNombreActionPerformed

    private void BTNSALIRADMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNSALIRADMIActionPerformed
        Eleccion1 m=new Eleccion1();
        m.setLocationRelativeTo(null);
        m.setVisible(true);
        dispose();       
    }//GEN-LAST:event_BTNSALIRADMIActionPerformed

    private void BTNAGREGARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNAGREGARActionPerformed
    if(!"".equals(TXTNombre.getText()) &&  !"".equals(TXTApellido.getText()) && !"".equals(TXTDNI.getText()) && !"".equals(TXTUsuario.getText()) &&
        !"".equals(TXTContraseña.getText()) &&TXTSexo.getSelectedItem()!="Seleccionar:" && !"".equals(TXTECivil.getText()) && TXTOcupacion.getSelectedItem()!="Seleccionar:" ){
        if((TXTUsuario.getText()).equals(TXTNombre.getText()+"."+ TXTDNI.getText())){
           int escoger =JOptionPane.showConfirmDialog(this,"Esta seguro que desea registrar","Confirmacion de registar",JOptionPane.YES_NO_OPTION);

                    switch(escoger){
                        case JOptionPane.YES_OPTION:
                            usuario.registrar(modelo);
                            break;
                        case JOptionPane.NO_OPTION:

                    } 
        }else{
           JOptionPane.showMessageDialog(null,"El nombre de usuario debe tener el formato : nombre.DNI ");
        }
    }else{
        JOptionPane.showMessageDialog(null,"No debe haber campos en blanco");
    }
        
        
    }//GEN-LAST:event_BTNAGREGARActionPerformed

    private void TXTContraseñaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTContraseñaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TXTContraseñaActionPerformed
    private void TXTApellidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTApellidoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TXTApellidoActionPerformed
    private void TXTECivilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTECivilActionPerformed

    }//GEN-LAST:event_TXTECivilActionPerformed
    private void TXTDNIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTDNIActionPerformed

    }//GEN-LAST:event_TXTDNIActionPerformed
    private void TXTUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTUsuarioActionPerformed
     
    }//GEN-LAST:event_TXTUsuarioActionPerformed
    private void TXTSexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTSexoActionPerformed

    }//GEN-LAST:event_TXTSexoActionPerformed

    private void BTNEDITARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNEDITARActionPerformed
        
        int i = TBADMI.getSelectedRow();
        
        TXTNombre.setText(modelo.getValueAt(i, 0).toString());
        TXTApellido.setText(modelo.getValueAt(i, 1).toString());
        TXTDNI.setText(modelo.getValueAt(i, 2).toString());
        TXTUsuario.setText(modelo.getValueAt(i, 3).toString());
        TXTContraseña.setText(modelo.getValueAt(i,4).toString());
        TXTSexo.setSelectedItem(modelo.getValueAt(i,5).toString());
        TXTECivil.setText(modelo.getValueAt(i, 6).toString());
        TXTOcupacion.setSelectedItem(modelo.getValueAt(i,7).toString());

    }//GEN-LAST:event_BTNEDITARActionPerformed

    private void BTNMODIFICARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNMODIFICARActionPerformed
        
        if((TXTUsuario.getText()).equals(TXTNombre.getText()+"."+ TXTDNI.getText())){
        usuario.editar(modelo);
        //PARA ACTUALIZAR LOS DATOS
        TXTNombre.setText("");
        TXTApellido.setText("");
        TXTDNI.setText("");
        TXTUsuario.setText("");
        TXTContraseña.setText("");
        TXTSexo.setSelectedItem("");
        TXTECivil.setText("");
        TXTSexo.setSelectedItem("");
        }else{
           JOptionPane.showMessageDialog(null,"El nombre de usuario debe tener el formato : nombre.DNI ");
        }

    }//GEN-LAST:event_BTNMODIFICARActionPerformed

    private void btnACTUALIZARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnACTUALIZARActionPerformed
        GestiondeUsuarios GestiondeUsuarios=new GestiondeUsuarios();
        GestiondeUsuarios.setLocationRelativeTo(null);
        GestiondeUsuarios.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnACTUALIZARActionPerformed

    private void TXTOcupacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTOcupacionActionPerformed

    }//GEN-LAST:event_TXTOcupacionActionPerformed

    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new GestiondeUsuarios().setVisible(true);
            }
        });
    }
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BTNAGREGAR;
    private javax.swing.JButton BTNEDITAR;
    private javax.swing.JButton BTNELIMINAR;
    private javax.swing.JButton BTNMODIFICAR;
    private javax.swing.JButton BTNSALIRADMI;
    public static javax.swing.JTable TBADMI;
    public static javax.swing.JTextField TXTApellido;
    public static javax.swing.JTextField TXTContraseña;
    public static javax.swing.JTextField TXTDNI;
    public static javax.swing.JTextField TXTECivil;
    public static javax.swing.JTextField TXTNombre;
    public static javax.swing.JComboBox<String> TXTOcupacion;
    public static javax.swing.JComboBox<String> TXTSexo;
    public static javax.swing.JTextField TXTUsuario;
    private javax.swing.JButton btnACTUALIZAR;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
