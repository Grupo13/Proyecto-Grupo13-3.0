
package estructura;

import javax.swing.JOptionPane;

public class Eleccion1 extends javax.swing.JFrame {

    public Eleccion1() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnCliente = new javax.swing.JButton();
        btnCliente1 = new javax.swing.JButton();
        btnAdministrador1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("INGRESE COMO :");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 50, -1, -1));

        btnCliente.setBackground(new java.awt.Color(51, 51, 51));
        btnCliente.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        btnCliente.setForeground(new java.awt.Color(255, 255, 255));
        btnCliente.setText("CLIENTE");
        btnCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClienteActionPerformed(evt);
            }
        });
        getContentPane().add(btnCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 270, 300, 86));

        btnCliente1.setBackground(new java.awt.Color(51, 51, 51));
        btnCliente1.setFont(new java.awt.Font("Comic Sans MS", 0, 24)); // NOI18N
        btnCliente1.setForeground(new java.awt.Color(255, 255, 255));
        btnCliente1.setText("SALIR");
        btnCliente1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCliente1ActionPerformed(evt);
            }
        });
        getContentPane().add(btnCliente1, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 410, -1, 25));

        btnAdministrador1.setBackground(new java.awt.Color(51, 51, 51));
        btnAdministrador1.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        btnAdministrador1.setForeground(new java.awt.Color(255, 255, 255));
        btnAdministrador1.setText("ADMINISTRADOR/USARIO");
        btnAdministrador1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdministrador1ActionPerformed(evt);
            }
        });
        getContentPane().add(btnAdministrador1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 110, 300, 88));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/main_bg3.jpg"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 450));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClienteActionPerformed
        Clientebusqueda m=new Clientebusqueda();
        m.setLocationRelativeTo(null);
        m.setVisible(true);
        dispose();
        JOptionPane.showMessageDialog(null,"Bienvenido :) \n \n"
                + "Usted podra observar nuestro registro total de psicologos \n"
                + "recomendados o segun el Distrito, Propuesta Salarial por hora,\n"
                + "Especialidad a tratar de su preferencia. \n"
                + "El objetivo del sistema es que usted pueda realizar una lista \n"
                + "de psicologos que le interesen y despues poder imprimirlos.\n"
                + "A continuacion se le detallara el manejo de este programa de\n "
                + "selección. \n"
                + "1. Seleccione un psicologo que le llame la atencion y\n"
                + "presionando seleccionar podra obtener la informacion \n"
                + "completa del psicologo si le agrada podra añadirlo a \n"
                + "su lista de psicologos de preferencia dando click en \n"
                + "el boton 'Añadir a mi lista de psicologos de preferencia'. \n"
                + "2. Podra realizar el primer paso las veces que quiera \n"
                + "Cuando ya haya acabado de escoger y desea poder generar \n"
                + "su reporte de la lista solo bastara con presionar el \n"
                + "boton 'Imprimir' y se habra generado su reporte en PDF \n"
                + "para imprimir luego.\n"
                );
    }//GEN-LAST:event_btnClienteActionPerformed

    private void btnCliente1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCliente1ActionPerformed
Inicio obj=new Inicio();
        obj.setLocationRelativeTo(null);
        obj.setVisible(true);
        dispose();       
    }//GEN-LAST:event_btnCliente1ActionPerformed

    private void btnAdministrador1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdministrador1ActionPerformed
        Eleccion2 obj=new Eleccion2();
        obj.setLocationRelativeTo(null);
        obj.setVisible(true);
        dispose();       
    }//GEN-LAST:event_btnAdministrador1ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Eleccion1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Eleccion1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Eleccion1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Eleccion1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Eleccion1().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdministrador1;
    private javax.swing.JButton btnCliente;
    private javax.swing.JButton btnCliente1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}
