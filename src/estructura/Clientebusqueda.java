package estructura;

import Metodos.conexion;
import Metodos.procedimientos_BD_BUSQUEDA;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Clientebusqueda extends javax.swing.JFrame {
    
//    procedimientos_BD_BUSQUEDA busqueda = new procedimientos_BD_BUSQUEDA();
    DefaultTableModel modelo = new DefaultTableModel();
    
    static Connection conn = null;
    //PARA LA CONEXION
    static Statement s = null;
    //PARA ESTABLECER UNA CONSULTA
    static ResultSet rs = null;
    //PARA BRINDAR UN RESULTADO
    
    int i;
    String REPORTECLIENTE;
   procedimientos_BD_BUSQUEDA p = new procedimientos_BD_BUSQUEDA();
    
    public Clientebusqueda() {
        initComponents();
        
         try {
            //ejecuta la conexion
            conn = conexion.Enlace(conn);
            
            //ejecuta la consulta
            rs = conexion.BUSQUEDA(rs);
            
            //volcamos los resultados de rs a rsmetadata
            ResultSetMetaData rsMd = rs.getMetaData();
           
            //La cantidad de columnas que tiene la TABLA
            int cantidadColumnas = rsMd.getColumnCount();
            
            //Establecer como cabezeras el nombre de las colunnas
            for (int j = 1; j <= cantidadColumnas; j++) {
                modelo.addColumn(rsMd.getColumnLabel(j));
            }
            rs.close();
            conn.close();
        } catch (SQLException ex) {
        }
        System.out.println("todo esta been");
        p.ObtenciondeDatos(modelo);
        
        TABBUSQCLIENTE.addMouseListener(new MouseAdapter(){
             
            @Override
            public void mouseClicked(MouseEvent e){
                 i= TABBUSQCLIENTE.getSelectedRow();   
            }
        });
        
         TABBUSQCLIENTE.getTableHeader().setReorderingAllowed(false) ;
//evita que se pueda mover las columnas cuando se ejecuta
         TABBUSQCLIENTE.setModel(new javax.swing.table.DefaultTableModel( 
            p.mostrar(),new String [] {"Nombre", "Apellido", "SEXO", "ESPECIALIDAD","DISTRITO","AÑOS DE \n EXPERIECIA","Propuesta \nSalarial"}
                                                                         )
                                );
         
        
        this.TABBUSQCLIENTE.setModel(modelo);
        
        //Reune los datos de cada psicologo en el nodo
        p.recolecciondedatos();
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jRadioButton1 = new javax.swing.JRadioButton();
        jComboBox1 = new javax.swing.JComboBox<>();
        jComboBox2 = new javax.swing.JComboBox<>();
        jComboBox3 = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        TABBUSQCLIENTE = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        jRadioButton1.setText("jRadioButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1000, 800));
        getContentPane().setLayout(null);

        jComboBox1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "10-20", "20-30", "30-40", "40-50", "50-60" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBox1);
        jComboBox1.setBounds(480, 140, 136, 30);

        jComboBox2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Distrito", "Ancon", "Ate", "Barranco", "Breña", "Carabayllo", "Chaclacayo", "Chorrillos", "Cieneguilla", "Comas", "El Agustino", "Independencia", "Jesús María", "La Molina", "La Victoria", "Lima", "Lince", "Los Olivos", "Lurigancho", "Lurín", "Magdalena del Mar", "Miraflores", "Pachacamac", "Pucusana", "Pueblo Libre", "Puente Piedra", "Punta Hermosa", "Punta Negra", "Rimac", "San Bartolo", "San Borja", "San Isidro", "San Juan de Lurigancho", "San Juan de Miraflores", "San Luis", "San Martín de Porres", "San Miguel", "Santa Anita", "Santa María del Mar", "Santa Rosa", "Santiago de Surco", "Surquillo", "Villa El Salvador" }));
        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBox2);
        jComboBox2.setBounds(150, 140, 210, 23);

        jComboBox3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Especialidad", "TERAPIA INDIVIDUAL", "TERAPIA DE PAREJAS", "TERAPIA EN ADICCION", "TERAPIA DE FAMILIA" }));
        jComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBox3);
        jComboBox3.setBounds(720, 140, 160, 30);

        TABBUSQCLIENTE.setAutoCreateRowSorter(true);
        TABBUSQCLIENTE.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        TABBUSQCLIENTE.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "NOMBRE", "APELLIDOS", "SEXO", "ESPECIALIDAD", "DISTRITO", "AÑOS DE EXPERIENCIA ", "PRESUPUESTO SALARILA", "GRADO ACADEMICO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TABBUSQCLIENTE.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        TABBUSQCLIENTE.setColumnSelectionAllowed(true);
        TABBUSQCLIENTE.setFocusable(false);
        TABBUSQCLIENTE.setMaximumSize(new java.awt.Dimension(2147483647, 320));
        TABBUSQCLIENTE.setMinimumSize(new java.awt.Dimension(655, 320));
        TABBUSQCLIENTE.setPreferredSize(new java.awt.Dimension(775, 320));
        TABBUSQCLIENTE.getTableHeader().setReorderingAllowed(false);
        TABBUSQCLIENTE.setUpdateSelectionOnSort(false);
        jScrollPane1.setViewportView(TABBUSQCLIENTE);
        TABBUSQCLIENTE.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (TABBUSQCLIENTE.getColumnModel().getColumnCount() > 0) {
            TABBUSQCLIENTE.getColumnModel().getColumn(0).setResizable(false);
            TABBUSQCLIENTE.getColumnModel().getColumn(1).setResizable(false);
            TABBUSQCLIENTE.getColumnModel().getColumn(2).setResizable(false);
            TABBUSQCLIENTE.getColumnModel().getColumn(3).setResizable(false);
            TABBUSQCLIENTE.getColumnModel().getColumn(4).setResizable(false);
            TABBUSQCLIENTE.getColumnModel().getColumn(5).setResizable(false);
            TABBUSQCLIENTE.getColumnModel().getColumn(6).setResizable(false);
            TABBUSQCLIENTE.getColumnModel().getColumn(7).setResizable(false);
        }

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(110, 190, 800, 320);

        jButton1.setBackground(new java.awt.Color(204, 204, 204));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes2/0/atras2.png"))); // NOI18N
        jButton1.setBorder(null);
        jButton1.setBorderPainted(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(760, 540, 150, 60);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes2/0/g2f.png"))); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton4);
        jButton4.setBounds(440, 540, 190, 60);

        jButton2.setBackground(new java.awt.Color(204, 204, 204));
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes2/0/seleccionarf2.png"))); // NOI18N
        jButton2.setBorder(null);
        jButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(80, 540, 230, 60);

        jLabel5.setFont(new java.awt.Font("Constantia", 1, 36)); // NOI18N
        jLabel5.setText("RELACIÓN DE PSICÓLOGOS DISPONIBLES");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(140, 10, 760, 60);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Especialidad");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(740, 110, 130, 22);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("Propuesta salarial");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(450, 110, 190, 22);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Distrito");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(220, 110, 70, 22);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes2/0/f1.png"))); // NOI18N
        getContentPane().add(jLabel4);
        jLabel4.setBounds(0, -50, 1030, 740);
        jLabel4.getAccessibleContext().setAccessibleName("jLabel4");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
            String variable = String.valueOf(jComboBox1.getSelectedItem());
               TABBUSQCLIENTE.setModel(new javax.swing.table.DefaultTableModel( 
            
            p.buscarXpsal(variable, p.mostrar().length)
            ,new String [] {"Nombre", "Apellido", "SEXO", "ESPECIALIDAD","DISTRITO","AÑOS DE \n EXPERIECIA","Propuesta \nSalarial"}


        ));     
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed
    String variable = String.valueOf(jComboBox2.getSelectedItem());
        

        TABBUSQCLIENTE.setModel(new javax.swing.table.DefaultTableModel( 
            
            p.buscarXdis(variable, p.mostrar().length),new String [] {"Nombre", "Apellido", "SEXO", "ESPECIALIDAD","DISTRITO","AÑOS DE \n EXPERIECIA","Propuesta \nSalarial"}


        ));     }//GEN-LAST:event_jComboBox2ActionPerformed

    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3ActionPerformed
 String variable = String.valueOf(jComboBox3.getSelectedItem());
        

        TABBUSQCLIENTE.setModel(new javax.swing.table.DefaultTableModel( 
            
            p.buscarXesp(variable, p.mostrar().length)
            ,new String [] {"Nombre", "Apellido", "SEXO", "ESPECIALIDAD","DISTRITO","AÑOS DE \n EXPERIECIA","Propuesta \nSalarial"}


        ));     }//GEN-LAST:event_jComboBox3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       
        int escoger = JOptionPane.showConfirmDialog(null, "¿Esta seguro que desea salir? \n Si sale se vaciara su lista de psicologos seleccionados", "Confirmacion de salida", JOptionPane.YES_NO_OPTION);
        
        switch (escoger) {
            case JOptionPane.YES_OPTION:
                p.eliminar(REPORTECLIENTE);
                Eleccion1 obj=new Eleccion1();
                obj.setLocationRelativeTo(null);
                obj.setVisible(true);
                dispose(); 
                break;
            case JOptionPane.NO_OPTION:

        }     
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
    p.seleccionar(i); 
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
    String ruta = "REPORTES\\reportecliente.jasper";
    p.Impresion(ruta);
    }//GEN-LAST:event_jButton4ActionPerformed



    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Clientebusqueda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Clientebusqueda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Clientebusqueda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Clientebusqueda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Clientebusqueda().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TABBUSQCLIENTE;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}

