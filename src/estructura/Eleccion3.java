
package estructura;

public class Eleccion3 extends javax.swing.JFrame {

    public Eleccion3() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCliente1 = new javax.swing.JButton();
        btnCliente = new javax.swing.JButton();
        btnAdministrador1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(800, 480));
        getContentPane().setLayout(null);

        btnCliente1.setBackground(new java.awt.Color(51, 51, 51));
        btnCliente1.setFont(new java.awt.Font("Comic Sans MS", 0, 24)); // NOI18N
        btnCliente1.setForeground(new java.awt.Color(255, 255, 255));
        btnCliente1.setText("SALIR");
        btnCliente1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCliente1ActionPerformed(evt);
            }
        });
        getContentPane().add(btnCliente1);
        btnCliente1.setBounds(633, 406, 107, 25);

        btnCliente.setBackground(new java.awt.Color(51, 51, 51));
        btnCliente.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        btnCliente.setForeground(new java.awt.Color(255, 255, 255));
        btnCliente.setText("VER REPORTES DEL DIA");
        btnCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClienteActionPerformed(evt);
            }
        });
        getContentPane().add(btnCliente);
        btnCliente.setBounds(241, 260, 300, 86);

        btnAdministrador1.setBackground(new java.awt.Color(51, 51, 51));
        btnAdministrador1.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        btnAdministrador1.setForeground(new java.awt.Color(255, 255, 255));
        btnAdministrador1.setText("GESTIONAR LOS PERFILES");
        btnAdministrador1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdministrador1ActionPerformed(evt);
            }
        });
        getContentPane().add(btnAdministrador1);
        btnAdministrador1.setBounds(241, 109, 300, 88);

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("QUE DESEA HACER:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(51, 12, 176, 26);

        jLabel2.setBackground(new java.awt.Color(51, 51, 51));
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/main_bg3.jpg"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 0, 780, 450);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCliente1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCliente1ActionPerformed
        Eleccion1 obj=new Eleccion1();
        obj.setLocationRelativeTo(null);
        obj.setVisible(true);
        dispose();        // TODO add your handling code here:
    }//GEN-LAST:event_btnCliente1ActionPerformed

    private void btnAdministrador1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdministrador1ActionPerformed
        Gperfiles m=new Gperfiles();
        m.setLocationRelativeTo(null);
        m.setVisible(true);
        dispose();         // TODO add your handling code here:
    }//GEN-LAST:event_btnAdministrador1ActionPerformed

    private void btnClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClienteActionPerformed
        Reportesdeldia m=new Reportesdeldia();
        m.setLocationRelativeTo(null);
        m.setVisible(true);
        dispose();         // TODO add your handling code here:
    }//GEN-LAST:event_btnClienteActionPerformed

    public static void main(String args[]) {
       
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Eleccion3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        

        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Eleccion3().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdministrador1;
    private javax.swing.JButton btnCliente;
    private javax.swing.JButton btnCliente1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}
